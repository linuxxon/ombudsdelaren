#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' 	Ombudsfördelaren

	Program för att göra ombudsfördelningar enligt uddatalsmetoden
	Följer Ung Vänsters stadgar antagna 2017

	För hjälp: Rasmus Linusson <rasmus@linusson.org>
'''

from __future__ import division

from pprint import pprint

# Tuple("Klubbnamn", Medlemmarstal, j_tal, mandat)

sthlm_klubbar=[
		("Gotland", 9, 9.0, 0),
		("Haninge-Tyresö-Nynäshamn", 19, 19.0, 0),
		("Hägersten-Skärholmen", 55, 55.0, 0),
		("Järva-Järfälla", 19, 19.0, 0),
		("Nacka-Värmdö", 32, 32.0, 0),
		("Norra innerstan", 41, 41.0, 0),
		("Norrort", 24, 24.0, 0),
		("Roslagen", 25, 25.0, 0),
		("Solna-Sundbyberg", 24, 24.0, 0),
		("Söder om Söder", 98, 98.0, 0),
		("Södermalm", 65, 65.0, 0),
		("Södertälje", 8, 8.0, 0),
		("Västerort", 39, 39.0, 0),
		("Västra Södertörn", 38, 38.0, 0)
		]

mandat=[29,39,49,59,69,79]
grundmandat=1

def dela_mandat():
	#sortera på störst jämförelsetal
	sthlm_klubbar.sort(key=lambda tup: tup[2], reverse=True)

	# Störst jämförelsetal får mandatet, räkna nytt jämförelsetal
	j_ny=sthlm_klubbar[0][1]/(1+2*(sthlm_klubbar[0][3]+1))

	sthlm_klubbar[0]=(sthlm_klubbar[0][0], sthlm_klubbar[0][1], j_ny, sthlm_klubbar[0][3]+1)

def main():
	for m in mandat:
		# Preppa grundmandat
		for i in range(0,len(sthlm_klubbar)):
			sthlm_klubbar[i] = (sthlm_klubbar[i][0], sthlm_klubbar[i][1], sthlm_klubbar[i][1], grundmandat)	

		for i in range(0,m-grundmandat*len(sthlm_klubbar)):
			dela_mandat()

		sthlm_klubbar.sort(key=lambda tup: tup[3], reverse=True)
		print "Mandat: ", m, "Grundmandat: ", grundmandat, "\n"
		for klubb in sthlm_klubbar:
			print klubb[0], "har ", klubb[3], " mandat"
		print("\n\n")

		# Återställ
		for i in range(0,len(sthlm_klubbar)):
			sthlm_klubbar[i] = (sthlm_klubbar[i][0], sthlm_klubbar[i][1], sthlm_klubbar[i][1], 0)	

if __name__ == "__main__":
	main()
